# Vera
#### A Fact Engine


## Installation - up and running
Using homebrew from the root of the project directory:
```shell
## install elixir and dependencies
brew bundle 

## fetch project dependencies -- you may be prompted to install hex and friends
mix deps.get

## build the application -- this will create a vera executable in the project root
MIX_ENV=prod mix escript.build

## give it a spin
./vera ./test/fixtures/examples/1/in.txt ./escript_out.txt

## check the output
cat ./escript_out.txt
```

## inspiration
![George Wendt and John Ratzenberger as Norm Peterson and Cliff Clavin on Cheers](https://media.giphy.com/media/Lkle0FaLEJxDSxmjlR/giphy.gif)
