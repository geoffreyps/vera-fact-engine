defmodule Vera.Factory do
  alias Vera.Expression

  def key do
    Enum.random(keys())
  end

  def keys() do
    [
      :is_a_cat,
      :is_a_plant,
      :is_a_boat,
      :is_a_house,
      :lassoed_a_tornado,
      :crossed_the_rubicon
    ]
  end

  def n_unique_of(fun, number) when is_function(fun, 0) and is_integer(number) and number > 0 do
    fun |> Stream.repeatedly() |> Stream.uniq() |> Enum.take(number)
  end

  def value do
    Enum.random(values())
  end

  def values() do
    [
      "garfield",
      "heathcliff",
      "felix",
      "pothos",
      "artemesia",
      "yarrow",
      "catamaran",
      "canoe"
    ]
  end

  def command do
    Enum.random(["INPUT", "QUERY"])
  end

  def command_atom do
    Enum.random(command_atoms())
  end

  def command_atoms do
    ~w(input query)a
  end

  def statement(arity) do
    case arity do
      1 -> arity_1_statement()
      2 -> arity_2_statement()
      3 -> arity_3_statement()
    end
  end

  def arity_1_statement do
    Enum.random(["is_a_cat", "is_a_boat", "is_a_plant"])
  end

  def arity_2_statement do
    Enum.random(["are_friends", "are_sympatico", "likes_cheese"])
  end

  def arity_3_statement do
    Enum.random(["are_triad", "are_musketeers", "make_a_triple"])
  end

  def to_string_expression(command, statement, args) do
    Enum.join([command, statement, args_to_string(List.wrap(args))], " ")
  end

  def args_to_string(args) do
    concatted =
      args
      |> Enum.reduce("", fn arg, acc ->
        acc <> arg <> "," <> " "
      end)
      |> String.trim_trailing(", ")

    "(#{concatted})"
  end

  def expression(arity \\ Enum.random(1..3), command \\ command_atom()) do
    %Expression{
      command: command,
      statement: statement(arity),
      args: values() |> Enum.shuffle() |> Enum.take(arity) |> List.to_tuple()
    }
  end
end
