defmodule Vera.ExpressionTest do
  use Vera.TestCase

  alias Vera.Expression

  describe "new/1" do
    for arity <- 1..3 do
      test "success: parsed expressions can be cast into structs (#{arity} arg expression)" do
        arity = unquote(arity)
        command = Factory.command()
        statement = Factory.statement(arity)
        values = Factory.values() |> Enum.take(arity)

        expected_output = %Expression{
          command: command |> String.downcase() |> String.to_atom(),
          statement: statement,
          args: List.to_tuple(values)
        }

        ## kickoff
        assert Expression.new(command: command, statement: statement, args: values) ==
                 {:ok, expected_output}
      end
    end
  end
end
