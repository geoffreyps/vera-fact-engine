defmodule Vera.TableTest do
  use Vera.TestCase

  alias Vera.Table
  @table Vera.Table

  describe "new" do
    setup do
      clear_table()
    end

    test "success: it creates a table" do
      ## setup

      expected_values = [
        name: Vera.Table,
        named_table: true,
        type: :duplicate_bag,
        protection: :protected
      ]

      ## kickoff
      table = Table.new()

      table_attrs = :ets.info(table)

      ## assertions
      for expected_value <- expected_values do
        assert expected_value in table_attrs
      end
    end
  end

  describe "insert/1" do
    setup do
      create_table()
      clear_table()
    end

    test "success: it inserts values into the table" do
      ## setup
      expected_key = Factory.key()
      expected_value = Factory.value()

      ## kickoff
      Table.insert(expected_key, {expected_value})

      ## assertions
      return_values = :ets.lookup(@table, expected_key)

      assert {expected_key, {expected_value}} in return_values
    end
  end

  describe "lookup/1" do
    setup do
      create_table()
      clear_table()
    end

    test "success: it retrieves information from the table for a given key (exercises duplicate bag)" do
      ## setup
      expected_key = Factory.key()

      expected_values = Factory.n_unique_of(&Factory.value/0, Enum.random(1..5))

      for expected_value <- expected_values do
        :ets.insert(@table, {expected_key, {expected_value}})
      end

      ## kickoff
      returned_values = Table.lookup(expected_key)

      for expected_value <- expected_values do
        assert {expected_key, {expected_value}} in returned_values
      end
    end

    test "success: it returns an empty list when nothing is set in the store" do
      ## setup
      [expected_key, dummy_key] = Enum.take(Factory.keys(), 2)
      dummy_values = Factory.n_unique_of(&Factory.value/0, Enum.random(1..4))
      ## put irrelevant data in the store
      for dummy_value <- dummy_values do
        :ets.insert(@table, {dummy_key, {dummy_value}})
      end

      ## kickoff / assertion
      assert [] == Table.lookup(expected_key)
    end
  end

  describe "handle_expression/1" do
    setup do
      create_table()
      clear_table()
    end

    for arity <- 1..3 do
      test "success: it inserts the input expression (#{arity}-args)" do
        arg_count = unquote(arity)

        expression = Factory.expression(arg_count, :input)

        ## kickoff
        assert {:ok, :inserted} == Table.handle_expression(expression)

        ## assertions -- is it in the cache?
        assert [{expression.statement, expression.args}] ==
                 :ets.lookup(@table, expression.statement)
      end
    end

    test "success: it can handle multiple input expressions with the same statement" do
      base_expression = Factory.expression(1, :input)

      expression1 = Map.put(base_expression, :args, {"garfield"})
      expression2 = Map.put(base_expression, :args, {"healthcliff"})

      ## kickoff
      for expression <- [expression1, expression2] do
        assert {:ok, :inserted} == Table.handle_expression(expression)
      end

      ## assertions -- are both values in the cache?
      returned_values = :ets.lookup(@table, base_expression.statement)

      for expression <- [expression1, expression2] do
        assert {expression.statement, expression.args} in returned_values
      end
    end

    test "success: it does explicit matches for true/false checks (single arguments)" do
      base_expression = Factory.expression(1, :input)

      expression1 = Map.put(base_expression, :args, {"garfield"})
      expression2 = Map.put(base_expression, :args, {"healthcliff"})

      ## exercise inserts
      for expression <- [expression1, expression2] do
        assert {:ok, :inserted} == Table.handle_expression(expression)
      end

      truthful_query_expression =
        base_expression |> Map.put(:command, :query) |> Map.put(:args, expression1.args)

      false_query_expression =
        base_expression |> Map.put(:command, :query) |> Map.put(:args, {"shouldn't be found"})

      ## kickoff
      assert {:ok, true} == Table.handle_expression(truthful_query_expression)

      assert {:ok, false} == Table.handle_expression(false_query_expression)
    end

    test "success: it can match patterns" do
      base_expression = Factory.expression(3, :input)

      ## exercise insert
      Table.handle_expression(base_expression)

      match_varx = "X"
      match_vary = "Y"

      {_x, keep, _y} = base_expression.args

      new_args = {match_varx, keep, match_vary}

      query_expression =
        base_expression
        |> Map.put(:args, new_args)
        |> Map.put(:command, :query)

      ## kickoff
      ## maybe change the return value
      assert {:ok,
              [[{"X:", elem(base_expression.args, 0)}, {"Y:", elem(base_expression.args, 2)}]]} ==
               Table.handle_expression(query_expression)
    end

    test "success: it can match patterns with same matcher" do
      [base_expression = %{args: {arg_to_repeat, _}} | _dummies] =
        for _ <- 1..5 do
          expression = Factory.expression(2, :input)
          Table.handle_expression(expression)

          expression
        end

      subject_expression = Map.put(base_expression, :args, {arg_to_repeat, arg_to_repeat})

      Table.handle_expression(subject_expression)

      match_vary = "Y"

      new_args = {match_vary, match_vary}

      query_expression =
        subject_expression
        |> Map.put(:args, new_args)
        |> Map.put(:command, :query)

      ## kickoff
      ## maybe change the return value
      assert {:ok,
              [
                [
                  {"Y:", elem(subject_expression.args, 0)}
                ]
              ]} ==
               Table.handle_expression(query_expression)
    end

    test "success: it returns false when there is no match" do
      base_expression = Factory.expression(3, :input)

      match_varx = "X"
      match_vary = "Y"

      {_x, keep, _y} = base_expression.args

      new_args = {match_varx, keep, match_vary}

      query_expression =
        base_expression
        |> Map.put(:args, new_args)
        |> Map.put(:command, :query)

      ## kickoff
      assert {:ok, false} ==
               Table.handle_expression(query_expression)
    end
  end

  defp create_table do
    Table.new()
  end

  defp clear_table do
    on_exit(fn ->
      case :ets.whereis(@table) do
        :undefined ->
          true

        _ ->
          :ets.delete_all_objects(Vera.Table)
      end
    end)
  end
end
