defmodule VeraTest do
  use Vera.TestCase
  @table Vera.Table

  describe "main/2" do
    setup do
      clear_table()
    end

    for example_number <- 1..4 do
      @tag :tmp_dir
      test "success: it reads an input file and writes to the provided path (example #{example_number})",
           %{tmp_dir: tmp_dir} do
        example_number = unquote(example_number)

        input_file = example_number_to_file(example_number, :in)
        output_file = tmp_dir_to_output_file(tmp_dir)

        ## kickoff
        Vera.main(input_file, output_file)

        expected_output = File.read!(example_number_to_file(example_number, :out))
        actual_output = File.read!(output_file)

        assert expected_output == actual_output
      end
    end
  end

  def example_number_to_file(n, type) when n > 0 and is_integer(n) and type in [:in, :out] do
    "./test/fixtures/examples/#{n}/#{type}.txt"
  end

  def tmp_dir_to_output_file(tmp_dir) do
    tmp_dir <> "/out.txt"
  end

  describe "handle_text/1" do
    setup do
      clear_table()
    end

    test "success: it returns expected output" do
      ## setup
      inputs = [
        "INPUT is_a_cat (lucy)",
        "INPUT is_a_cat (garfield)",
        "INPUT are_friends (alex, sam)",
        "INPUT is_a_cat (bowler_cat)",
        "INPUT are_friends (frog, toad)"
      ]

      Enum.map(inputs, &Vera.handle_text/1)

      expected_response = """
      ---
      true
      """

      ## kickoff
      assert expected_response == Vera.handle_text("QUERY is_a_cat (lucy)")
    end

    test "success: it handles data not in the store" do
      expected_response = """
      ---
      false
      """

      query = "QUERY is_a_cat (alf)"

      ## kickoff
      assert expected_response == Vera.handle_text(query)
    end

    test "success: it can query matches" do
      ## setup
      inputs = [
        "INPUT is_a_cat (lucy)",
        "INPUT is_a_cat (garfield)",
        "INPUT are_friends (alex, sam)",
        "INPUT is_a_cat (bowler_cat)",
        "INPUT are_friends (frog, toad)"
      ]

      Enum.map(inputs, &Vera.handle_text/1)

      expected_response = """
      ---
      X: alex
      """

      query = "QUERY are_friends (X, sam)"

      ## kickoff
      assert expected_response == Vera.handle_text(query)
    end

    test "success: with multiple matches" do
      ## setup
      inputs = [
        "INPUT is_a_cat (lucy)",
        "INPUT is_a_cat (garfield)",
        "INPUT are_friends (alex, sam)",
        "INPUT is_a_cat (bowler_cat)",
        "INPUT are_friends (frog, toad)"
      ]

      Enum.map(inputs, &Vera.handle_text/1)

      expected_response = """
      ---
      X: lucy
      X: garfield
      X: bowler_cat
      """

      query = "QUERY is_a_cat (X)"

      ## kickoff
      assert expected_response == Vera.handle_text(query)
    end

    test "success: it can query about pairs of friends" do
      ## setup
      inputs = [
        "INPUT is_a_cat (lucy)",
        "INPUT is_a_cat (garfield)",
        "INPUT are_friends (alex, sam)",
        "INPUT is_a_cat (bowler_cat)",
        "INPUT are_friends (frog, toad)"
      ]

      Enum.map(inputs, &Vera.handle_text/1)

      expected_response = """
      ---
      X: alex, Y: sam
      X: frog, Y: toad
      """

      query = "QUERY are_friends (X, Y)"

      ## kickoff
      assert expected_response == Vera.handle_text(query)
    end

    test "success: it can query repeated matches" do
      ## setup
      inputs = [
        "INPUT is_a_cat (lucy)",
        "INPUT is_a_cat (garfield)",
        "INPUT are_friends (alex, sam)",
        "INPUT is_a_cat (bowler_cat)",
        "INPUT are_friends (frog, toad)",
        "INPUT are_friends (sam, sam)"
      ]

      Enum.map(inputs, &Vera.handle_text/1)

      expected_response = """
      ---
      Y: sam
      """

      query = "QUERY are_friends (Y, Y)"

      ## kickoff
      assert expected_response == Vera.handle_text(query)
    end
  end

  defp clear_table do
    on_exit(fn ->
      case :ets.whereis(@table) do
        :undefined ->
          true

        _ ->
          :ets.delete_all_objects(Vera.Table)
      end
    end)
  end
end
