defmodule Vera.Table do
  @moduledoc """
  Access functions for Facts in memory.

  Wrapper functions for [Erlang Term Storage (ets)](https://www.erlang.org/doc/man/ets.html).
  """

  alias Vera.{Expression, MatchSpec}

  @table __MODULE__

  @spec new() :: __MODULE__ | reference()
  def new do
    case :ets.whereis(@table) do
      :undefined ->
        :ets.new(@table, [:named_table, :duplicate_bag])

      ref ->
        ref
    end
  end

  @spec insert(atom(), tuple()) :: tuple()
  def insert(key, values) when is_tuple(values) do
    :ets.insert(@table, {key, values})

    {:ok, :inserted}
  end

  @spec lookup(atom()) :: list(term())
  def lookup(key), do: :ets.lookup(@table, key)

  def handle_expression(%Expression{command: :input, statement: statement, args: args}) do
    insert(statement, args)
  end

  def handle_expression(
        %Expression{command: :query, statement: statement, args: args} = expression
      ) do
    return_value =
      with true <- MatchSpec.has_matches?(args),
           matches when matches != [] <-
             :ets.select(@table, MatchSpec.expression_to_ms(expression)) do
        matches
      else
        false ->
          results = lookup(statement)
          {statement, args} in results

        [] ->
          false
      end

    {:ok, return_value}
  end
end
