defmodule Vera.CLI do
  def main([input_file, output_file]) do
    Vera.main(input_file, output_file)
  end
end
