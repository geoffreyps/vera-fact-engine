defmodule Vera do
  @moduledoc """
  Documentation for `Vera`.
  """
  require Logger

  alias Vera.{Expression, Parser, Table}

  def main(input_path, output_path) do
    ensure_started()

    input_path |> read() |> write(output_path)
  end

  def read(path) do
    if File.exists?(path) do
      path
      |> Path.expand()
      |> File.stream!()
      |> Enum.map(&handle_text/1)
    end
  end

  def write(iodata, path) do
    File.write!(path, clean(iodata), [:exclusive])
  end

  def handle_text(string) when is_binary(string) do
    ensure_started()

    with %Vera.Expression{} = exp <- process_line(string) do
      exp
      |> Table.handle_expression()
      |> filter_response()
      |> List.wrap()
      |> Enum.reject(&is_nil/1)
      |> List.flatten()
      |> Enum.reject(fn
        [] -> true
        _ -> false
      end)
      |> Enum.join("\n")
      |> format_answer()
    end
  end

  def process_line(line) do
    with {:ok, expression, _, _, _, _} <- Parser.expression(line),
         {:ok, expression_struct} <- Expression.new(expression) do
      expression_struct
    else
      {:error, reason, text, _, _, _} -> {:error, {reason, text}}
      {:error, _reason} = error -> error
    end
  end

  defp filter_response([]), do: []
  defp filter_response([head]) when is_tuple(head), do: filter_response(head)

  defp filter_response([head | tail]) when is_tuple(head),
    do: [[filter_response(head), ", "], filter_response(tail)]

  defp filter_response([head | tail]), do: [filter_response(head) | filter_response(tail)]

  defp filter_response({:ok, :inserted}), do: nil
  defp filter_response({:ok, bool}) when is_boolean(bool), do: to_string(bool)
  defp filter_response({:ok, data}) when is_list(data), do: filter_response(data)

  defp filter_response(data) when is_tuple(data),
    do: data |> Tuple.to_list() |> filter_response() |> Enum.join(" ")

  defp filter_response(data) when is_binary(data), do: data

  defp format_answer([]), do: ""

  defp format_answer(iodata),
    do: "---\n#{to_string(iodata)}\n" |> clean()

  defp clean(iodata) do
    iodata
    |> to_string()
    |> String.replace("\n\n---", "")
    |> String.replace("\n, \n", ", ")
  end

  defp ensure_started, do: Vera.Table.new()
end
