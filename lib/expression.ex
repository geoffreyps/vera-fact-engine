defmodule Vera.Expression do
  defstruct [:command, :statement, :args]

  def new(params) when is_list(params) do
    with {:ok, casted_command} <- cast_command(params),
         {:ok, casted_args} <- cast_args(params) do
      {:ok,
       struct(__MODULE__,
         command: casted_command,
         args: casted_args,
         statement: params[:statement]
       )}
    else
      {:error, _reason} = error ->
        error
    end
  end

  defp command_to_atom("QUERY"), do: {:ok, :query}
  defp command_to_atom("INPUT"), do: {:ok, :input}
  defp command_to_atom(_), do: {:error, :bad_command}

  defp args_to_tuple([]), do: {:error, :no_args}
  defp args_to_tuple(args) when is_list(args), do: {:ok, List.to_tuple(args)}

  defp cast_command(keyword), do: keyword |> Keyword.get(:command) |> command_to_atom()
  defp cast_args(keyword), do: keyword |> Keyword.get(:args) |> args_to_tuple()
end
