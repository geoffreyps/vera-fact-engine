defmodule Vera.Parser do
  import NimbleParsec

  defcombinatorp(:input, string("INPUT"))
  defcombinatorp(:output, string("QUERY"))

  defcombinatorp(:open_paren, string("("))
  defcombinatorp(:close_paren, string(")"))
  defcombinatorp(:comma, string(","))
  defcombinatorp(:space, string("\s"))
  defcombinatorp(:newline, string("\n"))

  defparsec(
    :token,
    empty()
    |> ignore(optional(parsec(:space)))
    |> ascii_string([?a..?z, ?A..?Z, ?_, ?0..?9], min: 1)
  )

  defparsec(:arg, empty() |> ignore(optional(parsec(:comma))) |> parsec(:token))

  defparsec(
    :args,
    empty()
    |> ignore(optional(parsec(:space)))
    |> ignore(parsec(:open_paren))
    |> times(parsec(:arg), min: 1)
    |> tag(:args)
    |> parsec(:trailing)
  )

  defparsec(:trailing, ignore(parsec(:close_paren)) |> ignore(optional(parsec(:newline))))

  defparsec(:statement, parsec(:token) |> unwrap_and_tag(:statement))

  defparsec(:command, choice([parsec(:input), parsec(:output)]) |> unwrap_and_tag(:command))

  defparsec(:expression, parsec(:command) |> parsec(:statement) |> parsec(:args))
end
