defmodule Vera.MatchSpec do
  alias Vera.Expression

  def expression_to_ms(%Expression{command: :query, statement: statement, args: args}) do
    [
      {{statement, interpolate_vars(args)}, [], [selection_expression(args)]}
    ]
  end

  def has_matches?(args) do
    Enum.any?(Tuple.to_list(args), &starts_with_capital?/1)
  end

  @capital Regex.compile!("^([A-Z])")
  def starts_with_capital?(string) do
    Regex.match?(@capital, string)
  end

  def interpolate_vars(vars_tuple) do
    vars_tuple
    |> Tuple.to_list()
    |> Enum.with_index()
    |> flatten_vars()
    |> Enum.map(&maybe_to_var/1)
    |> List.to_tuple()
  end

  def flatten_vars(vars_with_index) do
    {_, flattened_vars} =
      Enum.reduce(vars_with_index, {%{}, []}, fn {var, index}, {seen, keeps} ->
        if seen[var] do
          {seen, [{var, seen[var]} | keeps]}
        else
          {Map.put(seen, var, index), [{var, index} | keeps]}
        end
      end)

    Enum.reverse(flattened_vars)
  end

  def maybe_to_var({string, idx}) when is_binary(string) do
    if starts_with_capital?(string) do
      to_var_name(string, idx)
    else
      string
    end
  end

  def to_var_name(_string, idx), do: :"$#{idx}"

  def selection_expression(args) do
    ## need to remove repeat capitalized strings, but preserve the index
    args
    |> Tuple.to_list()
    |> Enum.with_index()
    |> Enum.filter(fn {string, _idx} -> starts_with_capital?(string) end)
    |> Enum.uniq_by(fn {argname, _} -> argname end)
    |> Enum.map(fn {arg, index} -> {{"#{arg}:", to_var_name(arg, index)}} end)
    |> List.wrap()
  end
end
